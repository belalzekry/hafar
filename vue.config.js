module.exports = {
  runtimeCompiler: true, // <-------
  pluginOptions: {
    i18n: {
      locale: "en",
      fallbackLocale: "ar",
      localeDir: "locales",
      enableInSFC: false
    }
  },

};
